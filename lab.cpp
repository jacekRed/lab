//#include "lab.h"
#include <cstdlib>
using namespace std;

const int LEWA   = 1;
const int GORA   = 2;
const int PRAWA  = 4;
const int DOL    = 8;

int * stanDrzew;


struct TWsp{
    bool zajety;  //czy kierunek jest zajety
    bool mozliwy; //czy kierunek jest mozliwy/legalny
    int x1;
    int y1;
    int x2;
    int y2;
};

int** UtworzLabirynt(int wys,int szer){
    int i;
    int** lab;

    lab=new int*[szer];
    for(i=0;i!=wys;i++)
        lab[i] = new int[wys];

    return lab;
}

void UsunLabirynt(int wys,int szer, int** lab){
    int i;


    for(i=0;i!=wys;i++)
        delete [] lab[i];

    delete [] lab;

}

int ileDrzew(int SzerLab, int WysLab){
    int ile =0;
    int i;

    for (i=0;i<(SzerLab-1)*2 + (WysLab-1)*2;i++) {
        if (stanDrzew[i] == 1) ile++;
    }

    return ile;
}

int wylosujDrzewo(int SzerLab, int WysLab, int ile){
    int i;
    int ktora = ( rand() % ile );
    int licznik=0;
    int ileWolnych = ileDrzew(SzerLab,WysLab);

    //zabezpieczenie
    if( ktora > ileWolnych ) ktora = ileWolnych;

    for (i=0;i<(SzerLab-1)*2 + (WysLab-1)*2;i++) {
        if ((stanDrzew[i]==1) and (abs(ktora-licznik)<=1)){
            return i;
        }
        if (stanDrzew[i]==1) licznik++;
    }

    return ileWolnych;


}

void WspolrzedneRoota(int ktoreDrzewo,int SzerLab, int WysLab, TWsp* wsp){
    
    //gorny wiersz labiryntu to zakres od 0 do SzerLab-2
    //prawa kolumna labiryntu to zakres od SzerLab-2 + 1 do SzerLab-2+1 + WysLab-1
    //dolny wiersz labiryntu to zakres od SzerLab-2 + 1 do SzerLab-2+1 + WysLab-1+1 do SzerLab-2 + 1 do SzerLab-2+1 + WysLab-1+SzerLab-1
    //pozostale to lewa kolumna labiryntu
    if (ktoreDrzewo <= SzerLab-2) //gorny wiersz
    {
        wsp->x1=ktoreDrzewo;
        wsp->y1=0 ;
        wsp->x2=ktoreDrzewo+1;
        wsp->y2=0;
    } 
    else if (ktoreDrzewo <= SzerLab-2+ WysLab-1) //prawa kolumna
    {
        wsp->x1=SzerLab-1;
        wsp->y1=ktoreDrzewo-(SzerLab-1);
        wsp->x2=SzerLab-1;
        wsp->y2=ktoreDrzewo-(SzerLab-1)+1;
    } 
    else if (ktoreDrzewo <= (2*SzerLab-2+ WysLab-2)) //dolny wiersz
    {
        wsp->x1=2*SzerLab+WysLab - 4 - ktoreDrzewo;
        wsp->y1=WysLab-1;
        wsp->x2=2*SzerLab+WysLab - 4 - ktoreDrzewo + 1;
        wsp->y2=WysLab-1;
        
    } 
    else  //lewa kolumna
    {                                       
        wsp->x1=0;
        wsp->y1=2*(SzerLab + WysLab)- 5 - ktoreDrzewo;
        wsp->x2=0;
        wsp->y2=2*(SzerLab + WysLab)- 5 - ktoreDrzewo + 1;
    }    
}

int LosujKierunek(int rozmiar){
 return ( rand() % rozmiar );
}


bool CzyPionowo(TWsp* wsp){
    return (wsp->y1==wsp->y2);
}

//proba dodania galezi we wspolrzednych wsp
bool SprobujDodacGalaz(TWsp wsp, int** lab){
  bool pusta;
  //sprawdzenie czy jest to uklad komorek lewa-prawa czy gora-dol
  if (CzyPionowo(&wsp)){ //uklad lewa-prawa !!!!!!!!!!!!!!
      if (wsp.x1 < wsp.x2) { //komorka x1,y1 lezy na lewo od x2,y2
          pusta = ((lab[wsp.x1][wsp.y1] & PRAWA) == 0);
          if (pusta){
             lab[wsp.x1][wsp.y1] = lab[wsp.x1][wsp.y1] | PRAWA;
             lab[wsp.x2][wsp.y2] = lab[wsp.x2][wsp.y2] | LEWA;
          }
      }
      else { //komorka x2,y2 lezy na lewo od x1,y1
          pusta = ((lab[wsp.x2][wsp.y2] & PRAWA) == 0);
          if (pusta){
              lab[wsp.x1][wsp.y1] = lab[wsp.x1][wsp.y1] | LEWA;
              lab[wsp.x2][wsp.y2] = lab[wsp.x2][wsp.y2] | PRAWA;
          }
      }
  }
  else{ //uklad gora-dol
      if (wsp.y1 < wsp.y2) { //komorka x1,y1 lezy nad  x2,y2
          pusta = ((lab[wsp.x1][wsp.y1] & DOL) == 0);
          if (pusta){
              lab[wsp.x1][wsp.y1] = lab[wsp.x1][wsp.y1] | DOL;
              lab[wsp.x2][wsp.y2] = lab[wsp.x2][wsp.y2] | GORA;
          }
      }
      else { //komorka x2,y2 lezy nad  x1,y1
          pusta = ((lab[wsp.x2][wsp.y2] & DOL) == 0);
          if (pusta){
              lab[wsp.x1][wsp.y1] = lab[wsp.x1][wsp.y1] | GORA;
              lab[wsp.x2][wsp.y2] = lab[wsp.x2][wsp.y2] | DOL;
          }
      }
  }

  return pusta;


}

// zakladamy ze wsp okresla pozioma krawedz
//
//            ?|_|?
//
bool CzyLewa(TWsp* wsp, TWsp docelowa){
   return (docelowa.x1 < wsp->x1);
}

// zakladamy ze wsp okresla pozioma krawedz
//
//            ?-
//              |
//            ?-
//
bool CzyGora(TWsp* wsp, TWsp docelowa){
   return (docelowa.y1 > wsp->y1);
}



//sprawdza czy mozliwe/legalne jest rozszerzanie krawedzi wsp w okreslona strone
bool CzyMozliwa(TWsp* wsp, TWsp doceloweWsp, int strona,int SzerLab, int WysLab,int** lab){
  bool mozliwa = false;
  bool pionowa = CzyPionowo(wsp); //jaka krawedz badamy

  //w przypadku poziomych krawedzi i rozszerzen w gore/dol musimy wiedziec po jakiej stronie prawo/lewo znajduje sie dodelowa krawedz, ktora ma byc dodana
  //w przypadku pionowych krawedzi i rozszerzen w lewo/prawo musimy wiedziec po jakiej stronie prawo/lewo znajduje sie dodelowa krawedz, ktora ma byc dodana

  if ((strona == LEWA) and (pionowa)){  //czy moge w lewo od krawedzi pionej
      if (CzyGora(wsp,doceloweWsp)){ //docelowa lezy powyzej wsp
        mozliwa = (wsp->x1 > 1) and (wsp->y1 > 0);
        if (mozliwa) {mozliwa = ((lab[wsp->x1][wsp->y1] & GORA) + (lab[wsp->x1][wsp->y1] & LEWA) + (lab[wsp->x1-1][wsp->y1] & GORA) + (lab[wsp->x1-1][wsp->y1-1] & LEWA) == 0); }
      }
      else{ //docelowa lezy ponizej wsp
        mozliwa = (wsp->x1 > 0) and (wsp->y1 < WysLab-1);
        if (mozliwa) {mozliwa = ((lab[wsp->x1][wsp->y1] & LEWA) + (lab[wsp->x1][wsp->y1] & DOL) + (lab[wsp->x1-1][wsp->y1] & DOL) + (lab[wsp->x1][wsp->y1+1] & LEWA) == 0); }
      }
  }
  else if  ((strona == LEWA) and (!pionowa)){  // w lewo od krawedzi poziomej
      mozliwa = (wsp->x1 > 1)  and (wsp->y1 < WysLab-1);
      if (mozliwa) {mozliwa = ((lab[wsp->x1-1][wsp->y1] & DOL) + (lab[wsp->x1-1][wsp->y1] & LEWA) + (lab[wsp->x1-1][wsp->y1+1] & LEWA)+ (lab[wsp->x1-2][wsp->y1+1] & GORA) == 0); }
  }
  else if ((strona == PRAWA) and (pionowa)){
      if (CzyGora(wsp,doceloweWsp)){ //docelowa lezy powyzej wsp
        mozliwa = (wsp->x1 < SzerLab-2) and (wsp->y1 > 0) ; //!!!
        if (mozliwa) {mozliwa = ((lab[wsp->x2][wsp->y2] & GORA) + (lab[wsp->x2][wsp->y2] & PRAWA) + (lab[wsp->x2][wsp->y2-1] & PRAWA)+ (lab[wsp->x2+1][wsp->y2] & GORA) == 0); }
      }
      else{ //docelowa lezy ponizej wsp
        mozliwa = (wsp->x1 < SzerLab-2) and (wsp->y1 < WysLab-1); //!!!
        if (mozliwa) {mozliwa = ((lab[wsp->x2][wsp->y2] & DOL) + (lab[wsp->x2][wsp->y2] & PRAWA) + (lab[wsp->x2][wsp->y2+1] & PRAWA)+ (lab[wsp->x2+1][wsp->y2] & DOL) == 0); }
      }
  }
  else if ((strona == PRAWA) and (!pionowa)){
      mozliwa = (wsp->x1 < SzerLab-2);
      if (mozliwa) {mozliwa = ((lab[wsp->x1+1][wsp->y1] & DOL) + (lab[wsp->x1+1][wsp->y1] & PRAWA) + (lab[wsp->x1+1][wsp->y1+1] & PRAWA) + (lab[wsp->x1+2][wsp->y1] & DOL) == 0); }
  }
  else if ( (strona == GORA) and (pionowa)){
    mozliwa = (wsp->y1 > 1);
    if (mozliwa) {mozliwa = ((lab[wsp->x1][wsp->y1-1] & PRAWA) + (lab[wsp->x1][wsp->y1-1] & GORA) + (lab[wsp->x2][wsp->y2-1] & GORA)  + (lab[wsp->x2][wsp->y2-2] & LEWA)== 0); }
  }
  else if ( (strona == GORA) and (!pionowa)){
    if (CzyLewa(wsp,doceloweWsp)){ //docelowa lezy na lewo od wsp
      mozliwa = (wsp->y1 > 0) and (wsp->x1 >0);
      if (mozliwa) {mozliwa = ((lab[wsp->x1][wsp->y1] & GORA) + (lab[wsp->x1][wsp->y1] & LEWA) + (lab[wsp->x1-1][wsp->y1-1] & PRAWA)+ (lab[wsp->x1-1][wsp->y1-1] & DOL) == 0); }
    }
    else{//docelowa lezy na prawo od wsp
      mozliwa = (wsp->y1 > 0) and (wsp->x1 < SzerLab-1);
      if (mozliwa) {mozliwa = ((lab[wsp->x1][wsp->y1] & GORA) + (lab[wsp->x1][wsp->y1] & PRAWA) + (lab[wsp->x1][wsp->y1-1] & PRAWA)+ (lab[wsp->x1+1][wsp->y1-1] & DOL) == 0); }
    }
  }
  else if (pionowa){ //strona == DOL
      mozliwa = (wsp->y1 < WysLab-2);
      if (mozliwa) {mozliwa = ((lab[wsp->x1][wsp->y1+1] & PRAWA) + (lab[wsp->x1][wsp->y1+1] & DOL) + (lab[wsp->x1][wsp->y1+2] & PRAWA)+ (lab[wsp->x1+1][wsp->y1+1] & DOL) == 0) ; }
  }
  else { //strona == DOL i pozioma
    if (CzyLewa(wsp,doceloweWsp)){ //docelowa lezy na lewo od wsp
       mozliwa = (wsp->y1 < WysLab-2) and (wsp->x1 > 0); //!!!
       if (mozliwa) {mozliwa = ((lab[wsp->x2][wsp->y2] & LEWA) + (lab[wsp->x2][wsp->y2] & DOL) + (lab[wsp->x2-1][wsp->y2] & DOL) == 0) + (lab[wsp->x2-1][wsp->y2+1] & PRAWA); }
    }
    else{ //docelowa lezy na prawo od wp
      mozliwa = (wsp->y1 < WysLab-2) and (wsp->x1 < SzerLab - 1); //!!!
      if (mozliwa) {mozliwa = ((lab[wsp->x2][wsp->y2] & PRAWA) + (lab[wsp->x2][wsp->y2] & DOL) + (lab[wsp->x2+1][wsp->y2+1] & GORA) == 0) + (lab[wsp->x2+1][wsp->y2+1] & LEWA); }
    }
  }

  return mozliwa;

}

bool NaKrawedzi(TWsp wsp,int SzerLab, int WysLab){

    //gorny wiersz lub dolny wiersz lub lewa kolumna lub prawa kolumna
    return ((wsp.y1+wsp.y2 == 0) or (wsp.y1+wsp.y2 == 2*(WysLab-1) or (wsp.x1+wsp.x2 == 0) or (wsp.x1+wsp.x2 == 2*(SzerLab-1))));

}


//sprawdza mozliwosc ruchu w trzech kierunkach stojac na krawedzi pomiedzy
//wspolrzednymi w wsp
//root okresla to czy szukamy mozliwych kierunkow dla drzewa startujacego z boku labiryntu
int MozliweKierunki(TWsp* wsp, TWsp kierunki[],bool root,int SzerLab, int WysLab ,TWsp odWsp,int** lab){

    int i,licznik;
    bool moznaWdol  = false;
    bool moznaWprawo  = false;

    if (root) {  //dla nie wypelniamy tablicy kierunki; sprawdzamy tylko mozliwosc dodania i status zajetosci - jesli mozna dodac to zwracamy 1
       if (wsp->y1+wsp->y2 == 0) { //gorny wiersz
         wsp->mozliwy = ((lab[wsp->x1][wsp->y1] & DOL) + (lab[wsp->x2][wsp->y2] & DOL) + (lab[wsp->x1][1] & PRAWA) == 0);
         wsp->zajety  = ((lab[wsp->x1][wsp->y1] & PRAWA) != 0);
       }
       else if (wsp->y1==wsp->y2) { //dolny wiersz
         wsp->mozliwy = ((lab[wsp->x1][wsp->y1] & GORA) + (lab[wsp->x2][wsp->y2] & GORA) + (lab[wsp->x1][wsp->y1-1] & PRAWA) == 0);
         wsp->zajety  = ((lab[wsp->x1][wsp->y1] & PRAWA) != 0);
       }
       else if (wsp->x1+wsp->x2 == 0) { //lewa kolumna
         wsp->mozliwy = ((lab[wsp->x1][wsp->y1] & PRAWA) + (lab[wsp->x2][wsp->y2] & PRAWA) + (lab[1][wsp->y1] & DOL) == 0);
         wsp->zajety  = ((lab[wsp->x1][wsp->y1] & DOL) != 0);
       }
       else{ //prawa kolumna
         wsp->mozliwy = ((lab[wsp->x1][wsp->y1] & LEWA)  + (lab[wsp->x2][wsp->y2] & LEWA) + (lab[wsp->x1-1][wsp->y1] & DOL) == 0);
         wsp->zajety  = ((lab[wsp->x1][wsp->y1] & DOL) != 0);
       }

       if ((!wsp->zajety) and (wsp->mozliwy)) {return 1;}
       else {return 0;}

    }

    //wyznaczanie kierunkow poza rootem od wspolrzednych okreslonych przez strukture wsp
    //najpierw okreslenie mozliwych kierunkow

    //mozemy rosnac (teoretycznie) w dwoch kierunkach
    //przy czym jeden z tych kierunkow jest niedopuszczalny bo
    //od niego wychodzimy - namiary na ten niedopuszczalny/zrodlowy kierunek sa zawarte w strukturze odWsp
    //lub jesli odWsp jest NULL wowczas sparwdzamy na jakiej jestesmy krawedzi

    //Sprawdzamy czy kierunek jest gora-dol czy lewa-prawa

    if (CzyPionowo(wsp)){ //uklad lewa-prawa zatem krawedz jest pionowa !!!!!!!!!!!!!!!!!!!!

        //pozostaje sprawdzic vs odWsp czy probujemy rozszerzac powyzej czy ponizej wsp

        if (NaKrawedzi(odWsp,SzerLab,WysLab)){ //wsp wskazuje na krawedz gorna lub dolna
            moznaWdol = (wsp->y1+wsp->y2 == 0); //jestemy na gornej krawedzi
        }
        else { //wsp jest pomiedzy krawedzia gorna i dolna, punkt skad przychodzimy okresla odWsp
            moznaWdol = (odWsp.y1 < wsp->y1);
        }

        if (moznaWdol){ //idziemy w dol

          //kierunek na lewo
          kierunki[0].x1  = wsp->x1;
          kierunki[0].y1  = wsp->y1;
          kierunki[0].x2  = wsp->x1;
          kierunki[0].y2  = wsp->y1+1;
          kierunki[0].zajety  = ( (lab[wsp->x1][wsp->y1] & DOL) != 0 );
          kierunki[0].mozliwy = CzyMozliwa(wsp,kierunki[0],LEWA,SzerLab,WysLab,lab);

          //kierunek w dol
          kierunki[1].x1  = wsp->x1;
          kierunki[1].y1  = wsp->y1+1;
          kierunki[1].x2  = wsp->x2;
          kierunki[1].y2  = wsp->y2+1;
          kierunki[1].zajety  = ( (lab[wsp->x1][wsp->y1+1] & PRAWA) != 0 );
          kierunki[1].mozliwy = CzyMozliwa(wsp,kierunki[1],DOL,SzerLab,WysLab,lab);

          //kierunek na prawo
          kierunki[2].x1  = wsp->x2;
          kierunki[2].y1  = wsp->y2;
          kierunki[2].x2  = wsp->x2;
          kierunki[2].y2  = wsp->y2+1;
          kierunki[2].zajety  = ( (lab[wsp->x2][wsp->y2] & DOL) != 0 );
          kierunki[2].mozliwy = CzyMozliwa(wsp,kierunki[2],PRAWA,SzerLab,WysLab,lab);
        }
        else{ //idziemy w gore

          //kierunek na lewo
          kierunki[0].x1  = wsp->x1;
          kierunki[0].y1  = wsp->y1-1;
          kierunki[0].x2  = wsp->x1;
          kierunki[0].y2  = wsp->y1;
          kierunki[0].zajety  = ( (lab[wsp->x1][wsp->y1] & GORA) != 0 );
          kierunki[0].mozliwy = CzyMozliwa(wsp,kierunki[0],LEWA,SzerLab,WysLab,lab);

          //kierunek w gore
          kierunki[1].x1  = wsp->x1;
          kierunki[1].y1  = wsp->y1-1;
          kierunki[1].x2  = wsp->x2;
          kierunki[1].y2  = wsp->y2-1;
          kierunki[1].zajety  = ( (lab[wsp->x1][wsp->y1-1] & PRAWA) != 0 );
          kierunki[1].mozliwy = CzyMozliwa(wsp,kierunki[1],GORA,SzerLab,WysLab,lab);

          //kierunek na prawo
          kierunki[2].x1  = wsp->x2;
          kierunki[2].y1  = wsp->y2-1;
          kierunki[2].x2  = wsp->x2;
          kierunki[2].y2  = wsp->y2;
          kierunki[2].zajety  = ( (lab[wsp->x2][wsp->y2] & GORA) != 0 );
          kierunki[2].mozliwy = CzyMozliwa(wsp,kierunki[2],PRAWA,SzerLab,WysLab,lab);
        }

    }
    else { //uklad komorek gora dol wiec krawedz jest pozioma

        //pozostaje sprawdzic vs odWsp czy probujemy rozszerzac na lewo czy prawo od wsp

        if (NaKrawedzi(odWsp,SzerLab,WysLab)){ //wsp wskazuje na krawedz prawa lub lewa
            moznaWprawo = (wsp->x1+wsp->x2 == 0); //jestemy na lewej krawedzi
        }
        else { //wsp jest pomiedzy krawedzia lewa i prawa, punkt skad przychodzimy okresla odWsp
            moznaWprawo = (odWsp.x1 < wsp->x1);
        }

        if (moznaWprawo){ //idziemy w prawo

          //kierunek w gore
          kierunki[0].x1  = wsp->x1;
          kierunki[0].y1  = wsp->y1;
          kierunki[0].x2  = wsp->x1+1;
          kierunki[0].y2  = wsp->y1;
          kierunki[0].zajety  = ( (lab[wsp->x1][wsp->y1] & PRAWA) != 0 );
          kierunki[0].mozliwy = CzyMozliwa(wsp,kierunki[0],GORA,SzerLab,WysLab,lab);

          //kierunek w prawo
          kierunki[1].x1  = wsp->x1+1;
          kierunki[1].y1  = wsp->y1;
          kierunki[1].x2  = wsp->x2+1;
          kierunki[1].y2  = wsp->y2;
          kierunki[1].zajety  = ( (lab[wsp->x1+1][wsp->y1] & DOL) != 0 );
          kierunki[1].mozliwy = CzyMozliwa(wsp,kierunki[1],PRAWA,SzerLab,WysLab,lab);

          //kierunek w dol
          kierunki[2].x1  = wsp->x2;
          kierunki[2].y1  = wsp->y2;
          kierunki[2].x2  = wsp->x2+1;
          kierunki[2].y2  = wsp->y2;
          kierunki[2].zajety  = ( (lab[wsp->x2][wsp->y2] & PRAWA) != 0 );
          kierunki[2].mozliwy = CzyMozliwa(wsp,kierunki[2],DOL,SzerLab,WysLab,lab);

        }
        else { //idziemy w lewo

          //kierunek w gore
          kierunki[0].x1  = wsp->x1-1;
          kierunki[0].y1  = wsp->y1;
          kierunki[0].x2  = wsp->x1;
          kierunki[0].y2  = wsp->y1;
          kierunki[0].zajety  = ( (lab[wsp->x1][wsp->y1] & LEWA) != 0 );
          kierunki[0].mozliwy = CzyMozliwa(wsp,kierunki[0],GORA,SzerLab,WysLab,lab);

          //kierunek w lewo
          kierunki[1].x1  = wsp->x1-1;
          kierunki[1].y1  = wsp->y1;
          kierunki[1].x2  = wsp->x2-1;
          kierunki[1].y2  = wsp->y2;
          kierunki[1].zajety  = ( (lab[wsp->x1-1][wsp->y1] & DOL) != 0 );
          kierunki[1].mozliwy = CzyMozliwa(wsp,kierunki[1],LEWA,SzerLab,WysLab,lab);

          //kierunek w dol
          kierunki[2].x1  = wsp->x2-1;
          kierunki[2].y1  = wsp->y2;
          kierunki[2].x2  = wsp->x2;
          kierunki[2].y2  = wsp->y2;
          kierunki[2].zajety  = ( (lab[wsp->x2][wsp->y2] & LEWA) != 0 );
          kierunki[2].mozliwy = CzyMozliwa(wsp,kierunki[2],DOL,SzerLab,WysLab,lab);

        }
    }

    //liczymy ilosc mozliwych kierunkow
    licznik = 0;
    for(i=0;i<3;i++){
        if ((kierunki[i].mozliwy) or (kierunki[i].zajety)) licznik++;
    }

    return licznik;

}


bool dodajGalaz(TWsp wsp, int ktoreDrzewo,int SzerLab, int WysLab, TWsp odWsp, int** lab){
    TWsp kierunki[3];
    int ileKierunkow,i;
    int ktory,ktory2;
    
    //Wspolrzedne mozliwych kierunkow
    ileKierunkow = MozliweKierunki(&wsp,kierunki,false,SzerLab, WysLab, odWsp, lab);
    if (ileKierunkow == 3){
      ktory = LosujKierunek(3);
      if (SprobujDodacGalaz(kierunki[ktory],lab)) {return true;} //probujemy rozszerzac labirynt w pierwszym wylosowanym kierunku
      else if (dodajGalaz(kierunki[ktory],ktoreDrzewo,SzerLab, WysLab,wsp,lab)){return true;} //probujemy rozszerzac labirynt w pierwszym wylosowanym kierunku  !!!
      else{

          ktory2 = LosujKierunek(2);
          if (ktory==0) {  //pozostaja nam pozycje 1 i 2
              if (ktory2==1) {ktory=2;} //ktory2==1; ktory==2
              else {ktory = 1;}         //ktory2==2; ktory==1
          }
          else if (ktory==1) { //pozostaja nam pozycje 0 i 2
               if (ktory2==0) {ktory=2;} //ktory2==0; ktory==2
               else {ktory2=2; ktory=0;} //ktory2==2; ktory==0
          }
          else {              //pozostaja nam pozycje 0 i 1
              if (ktory2==0) {ktory=1;} //ktory2==0; ktory==1
              else {ktory2=1;ktory=0;}  //ktory2==1; ktory==0
          }

      }

      if (SprobujDodacGalaz(kierunki[ktory],lab)){return true;}  //probujemy rozszerzac labirynt w pierwszym pozostalym kierunku
      else if (dodajGalaz(kierunki[ktory],ktoreDrzewo,SzerLab, WysLab,wsp,lab)){return true;} //probujemy rozszerzac labirynt w pierwszym pozostalym kierunku
      else if (SprobujDodacGalaz(kierunki[ktory2],lab)){return true;} //probujemy rozszerzac labirynt w drugim pozostalym kierunku
      else if (dodajGalaz(kierunki[ktory2],ktoreDrzewo,SzerLab, WysLab,wsp,lab)){return true;} //probujemy rozszerzac labirynt w drugim pozostalym kierunku
      else return false;


    }
    else if(ileKierunkow == 2) {
      ktory = LosujKierunek(2);

      for(i=0;i<3;i++) { if (!(kierunki[i].mozliwy or kierunki[i].zajety)) {ktory2=i;}}

      if (ktory2==0) { //dopuszczalne 1 i 2
         if (ktory==0) {ktory=1;ktory2=2;}
         else          {ktory=2;ktory2=1;}
      }
      else if (ktory2==1) { //dopuszczalne 0 i 2
         if (ktory==0) {ktory=0;ktory2=2;}
         else          {ktory=2;ktory2=0;}

      }
      else { //dopuszczalne 0 i 1
         if (ktory==0) {ktory=0;ktory2=1;}
         else          {ktory=1;ktory2=0;}
      }

      if (SprobujDodacGalaz(kierunki[ktory],lab)){return true;}  //probujemy rozszerzac labirynt w pierwszym kierunku
      else if (dodajGalaz(kierunki[ktory],ktoreDrzewo,SzerLab, WysLab,wsp ,lab)){return true;} //probujemy rozszerzac labirynt w pierwszym kierunku
      else if (SprobujDodacGalaz(kierunki[ktory2],lab)){return true;} //probujemy rozszerzac labirynt w drugim kierunku
      else if (dodajGalaz(kierunki[ktory2],ktoreDrzewo,SzerLab, WysLab,wsp,lab)){return true;} //probujemy rozszerzac labirynt w drugim kierunku
      else return false;




    }
    else if(ileKierunkow == 1){
      for(i=0;i<3;i++) if (kierunki[i].mozliwy or kierunki[i].zajety)
      {
          if (SprobujDodacGalaz(kierunki[i],lab)){return true;}  //probujemy rozszerzac labirynt w jedynym mozliwym kierunku
          else {return false;} //
      }
    }
    else {return false;}


}

bool dodajGalazRoot(int ktoreDrzewo, int SzerLab, int WysLab, int** lab){

    TWsp wsp;
    TWsp kierunki[3];
    int ileKierunkow;

    //wspolrzedne roota
    WspolrzedneRoota(ktoreDrzewo, SzerLab, WysLab, &wsp);

    ileKierunkow = MozliweKierunki(&wsp,kierunki,true, SzerLab, WysLab, wsp, lab);

    if (ileKierunkow == 1){
      if (SprobujDodacGalaz(wsp,lab)){return true;}
    }
    else if (!wsp.zajety){return false;}

    //zajety wiec musimy eksplorowac dalej

    return dodajGalaz(wsp,ktoreDrzewo,SzerLab, WysLab, wsp ,lab);

}

void GenerujLabirynt(int SzerLab, int WysLab, int** lab){

    int ktoreDrzewo, i;

    stanDrzew = new int [(SzerLab-1)*2 + (WysLab-1)*2];

    for (i=0;i<(SzerLab-1)*2 + (WysLab-1)*2;i++) stanDrzew[i]=1;


    while (ileDrzew(SzerLab,WysLab) > 0){

        ktoreDrzewo = wylosujDrzewo(SzerLab,WysLab, ileDrzew(SzerLab,WysLab)+1);
        //ktoreDrzewo = 3;
        if (!dodajGalazRoot(ktoreDrzewo,SzerLab,WysLab,lab)){
            stanDrzew[ktoreDrzewo] = 0;
        }

    }


    delete [] stanDrzew;

}


void WyczyscLabirynt(int wys,int szer, int** lab){
    int i,j;

    for(i=0;i!=szer;i++)
      for(j=0;j!=wys;j++)
       lab[i][j] = 0;
}

