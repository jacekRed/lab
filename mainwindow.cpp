#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "lab.h"
#include <QStandardItem>
#include <string>
#include <ctime>
#include "kwadrat.h"

using namespace std;

bool rysuj = false;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    ui->Szerokosc->setValue(MIN_SZEROKOSC);
    ui->Wysokosc->setValue(MIN_WYSOKOSC);
    srand( time( NULL ) );

}

MainWindow::~MainWindow()
{
    UsunLabirynt(MAX_WYSOKOSC,MAX_SZEROKOSC,labirynt);
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    int SzerokoscLabiryntu, WysokoscLabiryntu, ii;
    QString ciag;
    int i,j;
    kwadrat kw;


   SzerokoscLabiryntu = ui->Szerokosc->value();
   WysokoscLabiryntu  = ui->Wysokosc->value();

   labirynt = UtworzLabirynt(MAX_WYSOKOSC,MAX_SZEROKOSC);

   WyczyscLabirynt(MAX_WYSOKOSC,MAX_SZEROKOSC,labirynt);

   GenerujLabirynt(SzerokoscLabiryntu,WysokoscLabiryntu, labirynt);

   //QTableWidgetItem *firstRow = new QTableWidgetItem(QString::number(labirynt[0][0]));
   //ui->tableWidget->setItem(0,0,firstRow);

   for(i=0;i<SzerokoscLabiryntu;i++)
       for(j=0;j<WysokoscLabiryntu;j++)
   //firstRow = new QTableWidgetItem(QString::number(labirynt[i][j]));
   ui->tableWidget->setItem(j,i,new QTableWidgetItem(QString::number(labirynt[i][j])));

   rysuj = true;
   update();

}

void MainWindow::paintEvent(QPaintEvent *e) {
  QPainter painter(this);
  QPen penRed(Qt::red,2);
  QPen penBlack(Qt::black,1);


  int rogX, rogY, wysK, szerK,i,j;
  int SzerokoscLabiryntu = ui->Szerokosc->value();
  int WysokoscLabiryntu  = ui->Wysokosc->value();

  if (rysuj){

    //QRectF rectangle(10.0, 200.0, 80.0, 60.0);
    //int startAngle = 30 * 16;
    //int spanAngle = 120 * 16;
    rogX = 20;
    rogY = 200;
    wysK = 10;
    szerK = 10;
    SzerokoscLabiryntu = ui->Szerokosc->value();
    WysokoscLabiryntu  = ui->Wysokosc->value();




    painter.setBrush(QBrush(Qt::red));
    painter.setPen(penRed);
    painter.drawLine(rogX,rogY,rogX+SzerokoscLabiryntu*szerK,rogY); //gora
    painter.drawLine(rogX,rogY,rogX,rogY+WysokoscLabiryntu*wysK); //lewa

    painter.drawLine(rogX,rogY+WysokoscLabiryntu*wysK,rogX+SzerokoscLabiryntu*szerK,rogY+WysokoscLabiryntu*wysK); //dol
    painter.drawLine(rogX+SzerokoscLabiryntu*szerK,rogY,rogX+SzerokoscLabiryntu*szerK,rogY+WysokoscLabiryntu*wysK); //prawa

    //painter.drawPolygon(QPolygon(rogX,rogY,rogX+szerK,2,3,4));

    painter.setPen(penBlack);
    painter.setBrush(Qt::black);
    //rysujemy kwadraciki
    for(i=0;i<SzerokoscLabiryntu;i++)
        for(j=0;j<WysokoscLabiryntu;j++){
          if((labirynt[i][j]) & (1))  painter.drawLine(rogX+szerK*(i),rogY+wysK*(j),rogX+szerK*(i),rogY+wysK*(j+1));
          if((labirynt[i][j]) & (4))  painter.drawLine(rogX+szerK*(i+1)    ,rogY+wysK*(j),rogX+szerK*(i+1)    ,rogY+wysK*(j+1));
          if((labirynt[i][j]) & (2))  painter.drawLine(rogX+szerK*(i),rogY+wysK*(j),rogX+szerK*(i+1)    ,rogY+wysK*(j));
          if((labirynt[i][j]) & (8))  painter.drawLine(rogX+szerK*(i),rogY+wysK*(j+1)    ,rogX+szerK*(i+1)    ,rogY+wysK*(j+1));
        }


  }
}


void MainWindow::on_pushButton_2_clicked()
{
    int aa,bb,wy,ww,wy1,ww1;

    ui->wynik->setText(QString("dddd"));

    aa = (ui->liczbaA->text()).toInt();
    bb = (ui->liczbaB->text()).toInt();

    wy = (aa and bb);
    ww = (aa or bb);

    wy1 = (aa & bb);
    ww1 = (aa | bb);

    ui->wynik->setText(QString(wy));

    //qApp->exit();
}
