#include "kwadrat.h"
#include <QPainter>

kwadrat::kwadrat(QWidget *parent) :
    QWidget(parent)
{


}

void kwadrat::paintEvent(QPaintEvent *e)
{
  Q_UNUSED(e);
  QPainter qp(this);
  drawBox(&qp);
}

void kwadrat::drawBox(QPainter *qp)
{
  QPen pen(Qt::black, 2, Qt::SolidLine);
  qp->setPen(pen);
  qp->drawLine(20, 40, 250, 40);
}
