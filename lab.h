#ifndef LAB_H
#define LAB_H

const int PUSTY   = 0; ///Galaz nie istnieje
const int OTWARTY = 1; ///Galaz istnieje ale moze sie rozrastac
const int PELNY   = 2; ///Galaz istnieje ale nie moze sie juz rozrastac

const int MAX_SZEROKOSC = 100;
const int MAX_WYSOKOSC = 100;

const int MIN_SZEROKOSC = 2;
const int MIN_WYSOKOSC = 2;

struct TGra{
    int x; ///Wspolrzedne pozycji - szerokosc
    int y; ///Wspolrzedne pozycji - wysokosc
    //czas ///Czas wykonania ruchu
    TGra *next; /// wskaznik do nastepnika
    TGra *prev; ///wskaznik do poprzednika
};



int ** labirynt; ///dynamiczna macierz dwuwymiarowa zawierająca informacje o labiryncie

///Funkcja generująca labirynt
int GenerujLabirynt(int SzerLab, int WysLab, int** lab);

///Funkcja Zerująca definicje labiryntu bez zwalniania pamięci!
void WyczyscLabirynt(int wys,int szer, int** lab);

///Zwalnianie pamieci po labiryncie - uzywane tylko przy zamykaniu programu
void UsunLabirynt(int wys,int szer, int** lab);

int** UtworzLabirynt(int wys,int szer);




//ObliczNajkrotszaDroge;
//

#endif // LAB_H
