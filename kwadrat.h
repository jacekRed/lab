#ifndef KWADRAT_H
#define KWADRAT_H

#include <QWidget>

class kwadrat : public QWidget
{
    Q_OBJECT
public:
    explicit kwadrat(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *event);
    void drawBox(QPainter *qp);
signals:

public slots:

};

#endif // KWADRAT_H
