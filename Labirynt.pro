#-------------------------------------------------
#
# Project created by QtCreator 2014-03-08T17:39:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Labirynt
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    lab.cpp \
    kwadrat.cpp

HEADERS  += mainwindow.h \
    lab.h \
    kwadrat.h

FORMS    += mainwindow.ui
